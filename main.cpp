#include <QCoreApplication>
#include <QTimer>
#include "qtsearchprocess.h"


int main(int argc, char *argv[])
{
//    qSetMessagePattern("%{file}(%{line}): %{message}");
    QCoreApplication a(argc, argv);

    QtSearchProcess search;

    // Quit application when work is finished
    QObject::connect(&search, &QtSearchProcess::finished, &a, &QCoreApplication::quit); // changed the
    //variable name 'app' to 'a'

    // Run the user-hook (doDownload) from the application event loop.
    QTimer::singleShot(0, &search, &QtSearchProcess::onSearchButton);

    return a.exec();
}
