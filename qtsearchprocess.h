#ifndef QTSEARCHPROCESS_H
#define QTSEARCHPROCESS_H

#include <QObject>
#include <QProcess>

class QtSearchProcess : public QObject
{
    Q_OBJECT
public:
    explicit QtSearchProcess(QObject *parent = nullptr);

signals:
    void finished();
public slots:
    void onSearchButton();
    void showResult(int exitCode, QProcess::ExitStatus exitStatus);
    void onReadResult();
    void onReadError();

private:
    QProcess *process;
    QString m_result;
};

#endif // QTSEARCHPROCESS_H
